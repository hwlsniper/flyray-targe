package me.flyray.common.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Author: bolei
 * @date: 16:32 2018/12/6
 * @Description: 分页公共请求
 */

@Data
public class PaginationParam {



    @ApiModelProperty(value = "当前页码")
    @NotNull(message = "当前页码不能为空")
    private int currentPage;

    @ApiModelProperty(value = "每页条数")
    @NotNull(message = "每页条数不能为空")
    private int pageSize;

}
