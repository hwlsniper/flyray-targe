package me.flyray.common.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 查询用户基础信息请求实体
 * @author centerroot
 * @time 创建时间:2018年7月16日下午4:23:01
 * @description
 */
@Data
@ApiModel(value = "客户详情查询参数请求")
public class PersonalDetailRequest implements Serializable {

	@NotNull(message = "参数[personalId]不能为空")
	@ApiModelProperty(value = "个人客户编号")
	private String personalId;
}
