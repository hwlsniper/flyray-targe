package me.flyray.common.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import me.flyray.common.enums.AccountType;

import java.io.Serializable;


/**
 * 个人客户信息
 * @author centerroot
 * @time 创建时间:2018年8月14日下午2:47:21
 * @description
 */

@Data
public class QueryPersonalAccountRequest implements Serializable {

    @ApiModelProperty(value = "平台编号")
    private String platformId;

    @ApiModelProperty(value = "个人客户编号")
    private String personalId;

    /** 积分余额账户*/
    private final static String pointAccount = AccountType.POINT.getCode();

    /**积分历史账户*/
    private final static String pointAccountHis =  AccountType.HIS_POINT.getCode();



}
