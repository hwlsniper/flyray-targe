package me.flyray.common.vo.admin;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @Author: bolei
 * @date: 10:05 2019/2/14
 * @Description: 添加admin用户请求
 */

@Data
public class AdminUserRequest implements Serializable {

    private String userId;

    @NotNull(message="登陆用户名不能为空")
    private String username;

    @NotNull(message="登陆密码不能为空")
    private String password;

    /**
     * 所属部门机构ID
     */
    @NotNull(message="所属部门机构ID不能为空")
    private String deptId;

    /**
     * 所属部门机构名称
     */
    @NotNull(message="所属部门机构名称不能为空")
    private String deptName;

    private String birthday;

    private String address;

    private String mobilePhone;

    private String telPhone;

    private String email;

    private String sex;

    private String status;

    private String description;

    private Date crtTime;

    private String crtUser;

    private String crtName;

    private String crtHost;

    private Date updTime;

    private String updUser;

    private String updName;

    private String updHost;

    /**
     * 平台编号
     */
    @NotNull(message="所属平台不能为空")
    private String platformId;

    /**
     * 商户编号
     */
    private String merchantId;

    /**
     * 用户权限1，系统管理员2，平台管理员3，商户管理员4，平台操作员
     */
    @NotNull(message="用户类型不能为空")
    private Integer userType;

    /**
     * 用户业务角色类型
     */
    private Integer userBizRole;

    /**
     * 省
     */
    private String areaCode;

    /**
     * 市
     */
    private String areaName;

    /**
     * 县
     */
    private String areaLayer;

    @ApiModelProperty(value = "当前页码")
    private int page;

    @ApiModelProperty(value = "每页条数")
    private int limit;

    @ApiModelProperty(value = "直接主管名称")
    private String parentName;

}
