package me.flyray.common.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 更新客户经理
 * @author centerroot
 * @time 创建时间:2018年7月16日下午4:23:01
 * @description
 */
@Data
@ApiModel(value = "修改客户经理")
public class PersonalOwnerRequest implements Serializable {

	@NotNull(message = "参数[ownerId]不能为空")
	@ApiModelProperty(value = "客户经理id")
	private String ownerId;

	@NotNull(message = "参数[ownerName]不能为空")
	@ApiModelProperty(value = "客户经理名称")
	private String ownerName;

	@Size(max = 50 )
	@NotNull(message = "参数[personalIds]不能为空")
	@ApiModelProperty(value = "个人客户编号列表")
	private String[] personalIds;
}
