package me.flyray.common.rest;

import me.flyray.common.biz.BaseBiz;
import me.flyray.common.context.BaseContextHandler;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.msg.TableResultResponse;
import me.flyray.common.util.Query;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;


/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-15 8:48
 */
@Slf4j
public class BaseController<Biz extends BaseBiz,Entity> {
    @Autowired
    protected HttpServletRequest request;
    @Autowired
    protected Biz baseBiz;
    @Value("${toplevel.platform.id}")
	private String topLevelPlatformId;
    
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse<Entity> add(@RequestBody Entity entity){
    	log.debug("BaseController add.....");
        baseBiz.insertSelective(entity);
        return BaseApiResponse.newSuccess(entity);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    @ResponseBody
    public BaseApiResponse<Entity> get(@PathVariable String id){
        Object o = baseBiz.selectById(id);
        return  BaseApiResponse.newSuccess((Entity)baseBiz.selectById(id));
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    @ResponseBody
    public BaseApiResponse<Entity> update(@RequestBody Entity entity, @PathVariable int id){
    	log.debug("BaseController update.....");
        baseBiz.updateSelectiveById(entity);
        return BaseApiResponse.newSuccess(entity);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public BaseApiResponse<Entity> remove(@PathVariable String id){
        baseBiz.deleteById(id);
        return BaseApiResponse.newSuccess();
    }

    @RequestMapping(value = "/all",method = RequestMethod.GET)
    @ResponseBody
    public List<Entity> all(){
        return baseBiz.selectListAll();
    }

    @RequestMapping(value = "/page",method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<Entity> list(@RequestParam Map<String, Object> params){
        //查询列表数据
    	params.remove("userType");
    	for (String key : params.keySet()) {
    		Object value = params.get(key);
    		if (StringUtils.isEmpty(value) 
    				|| ("platformId".equals(key) && topLevelPlatformId.equals(params.get(key)))) {
    			params.remove(key);
			}
    	}
        Query query = new Query(params);
        return baseBiz.selectByQuery(query);
    }

    public String getCurrentUserName(){
        return BaseContextHandler.getXName();
    }
    
    public String setPlatformId(String platformId){
    	if (!topLevelPlatformId.equals(platformId)) {
    		return platformId;
		}
		return null;
    }
    
    public static void main(String[] args) {
    	Object value = "33";
    	String key = "platformId";
    	System.out.println(StringUtils.isEmpty(value) 
				|| ("platformId".equals(key) && "1001".equals("1001")));
    }
    
}
