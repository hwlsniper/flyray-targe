package me.flyray.common.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * @Author: bolei
 * @date: 17:44 2019/1/9
 * @Description: 货币单位 数据类型转换
 */

public class CurrencyUtils {

    public static String decimaltoString(BigDecimal amt){
        DecimalFormat format = new DecimalFormat("0.00");
        return format.format(amt);
    }



}
