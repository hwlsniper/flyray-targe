package me.flyray.auth.feignserver;

import me.flyray.auth.configuration.KeyConfiguration;
import me.flyray.auth.service.AuthClientService;
import me.flyray.common.msg.BaseApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * bolei
 */
@RestController
@RequestMapping("client")
@Slf4j
public class ClientController {
    @Autowired
    private AuthClientService authClientService;
    @Autowired
    private KeyConfiguration keyConfiguration;

    @RequestMapping(value = "/token", method = RequestMethod.POST)
    public BaseApiResponse<String> getAccessToken(String clientCode, String secret) throws Exception {
        return BaseApiResponse.newSuccess(authClientService.apply(clientCode, secret));
    }

    @RequestMapping(value = "/myClient")
    public BaseApiResponse<List<String>> getAllowedClient(String serviceId, String secret) {
        return BaseApiResponse.newSuccess(authClientService.getAllowedClient(serviceId, secret));
    }

    @RequestMapping(value = "/servicePubKey",method = RequestMethod.POST)
    public BaseApiResponse<byte[]> getServicePublicKey(@RequestParam("clientId") String clientId, @RequestParam("secret") String secret) throws Exception {
        authClientService.validate(clientId, secret);
        return BaseApiResponse.newSuccess(keyConfiguration.getServicePubKey());
    }

    @RequestMapping(value = "/userPubKey",method = RequestMethod.POST)
    public BaseApiResponse<byte[]> getUserPublicKey(@RequestParam("clientId") String clientId, @RequestParam("secret") String secret) throws Exception {
        authClientService.validate(clientId, secret);
        return BaseApiResponse.newSuccess(keyConfiguration.getUserPubKey());
    }


}
