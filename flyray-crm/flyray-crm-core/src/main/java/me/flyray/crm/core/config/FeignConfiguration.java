package me.flyray.crm.core.config;

import me.flyray.crm.core.interceptor.UserTokenInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * Created by ace on 2017/9/12.
 */
@Configuration
public class FeignConfiguration {
    @Bean
    UserTokenInterceptor getClientTokenInterceptor(){
        return new UserTokenInterceptor();
    }
}
