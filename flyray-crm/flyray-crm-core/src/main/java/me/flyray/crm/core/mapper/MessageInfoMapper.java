package me.flyray.crm.core.mapper;

import me.flyray.crm.core.entity.MessageInfo;
import tk.mybatis.mapper.common.Mapper;

/**
 * 消息表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-08-21 17:35:49
 */
@org.apache.ibatis.annotations.Mapper
public interface MessageInfoMapper extends Mapper<MessageInfo> {
	
}
