package me.flyray.crm.core.biz.merchant;

import me.flyray.common.biz.BaseBiz;
import me.flyray.crm.core.entity.MerchantAccountJournal;
import me.flyray.crm.core.mapper.MerchantAccountJournalMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * 商户账户流水（充、转、提、退、冻结流水）
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Service
public class MerchantAccountJournalBiz extends BaseBiz<MerchantAccountJournalMapper, MerchantAccountJournal> {
	
	private static final Logger logger= LoggerFactory.getLogger(MerchantAccountJournalBiz.class);

}