package me.flyray.crm.core.api;

import java.util.Map;

import javax.validation.Valid;

import me.flyray.common.msg.BaseApiResponse;
import me.flyray.crm.core.entity.CustomerBase;
import me.flyray.crm.facade.request.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.crm.core.biz.customer.CustomerBaseBiz;
import me.flyray.crm.core.biz.personal.PersonalCertificationInfoBiz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags="客户基础信息管理")
@Controller
@RequestMapping("customer")
public class CustomerController {

	@Autowired
	private CustomerBaseBiz customerBaseBiz;
	@Autowired
	private PersonalCertificationInfoBiz personalCertificationInfoBiz;
	/**
	 * 用户注册
	 * @author centerroot
	 * @time 创建时间:2018年9月7日下午5:00:32
	 * @param personalBaseRequest
	 * @return
	 */
	@ApiOperation("用户注册")
	@RequestMapping(value = "/register",method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse register(@RequestBody @Valid RegisterRequest registerRequest) {
		CustomerBase response = customerBaseBiz.register(registerRequest);
		return BaseApiResponse.newSuccess(response);
    }
	
	/**
	 * 用户登录
	 * @author centerroot
	 * @time 创建时间:2018年9月7日下午5:00:32
	 * @param loginRequest
	 * @return
	 */
	@ApiOperation("用户登录")
	@RequestMapping(value = "/login",method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse login(@RequestBody @Valid LoginRequest loginRequest) {
		CustomerBase response = customerBaseBiz.login(loginRequest);
		return BaseApiResponse.newSuccess(response);
    }
	
	/**
	 * 用户实名认证
	 * */
	@ApiOperation("用户实名认证")
	@RequestMapping(value = "/certification",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> certification(@RequestBody @Valid CertificationRequest certificationRequest) {
        Map<String, Object> response = personalCertificationInfoBiz.certification(certificationRequest);
		return response;
    }
	
	/**
	 * realNameQuery 实名认证查询接口
	 * @author centerroot
	 * @time 创建时间:2018年9月25日上午9:16:55
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/realNameQuery", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> realNameQuery(@RequestBody @Valid CertificationRequest certificationRequest) {
		return personalCertificationInfoBiz.realNameQuery(certificationRequest);
	}
	
	/**
	 * setupPayPassword 设置/重置交易密码
	 * @author centerroot
	 * @time 创建时间:2018年9月25日上午10:54:44
	 * @param customerSetupPasswordRequest
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setupPayPassword", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> setupPayPassword(@RequestBody @Valid CustomerSetupPasswordRequest customerSetupPasswordRequest) {
		return customerBaseBiz.setupPayPassword(customerSetupPasswordRequest);
	}

	/**
	 * verifyPayPassword 校验交易密码
	 * @author centerroot
	 * @time 创建时间:2018年9月25日上午10:54:37
	 * @param customerVerifyPasswordRequest
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/verifyPayPassword", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> verifyPayPassword(@RequestBody @Valid CustomerVerifyPasswordRequest customerVerifyPasswordRequest) {
		return customerBaseBiz.verifyPayPassword(customerVerifyPasswordRequest);
	}

	/**
	 * updatePayPassword 修改交易密码
	 * @author centerroot
	 * @time 创建时间:2018年9月25日上午10:54:30
	 * @param customerUpdatePasswordRequest
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updatePayPassword", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> updatePayPassword(@RequestBody @Valid CustomerUpdatePasswordRequest customerUpdatePasswordRequest) {
		return customerBaseBiz.updatePayPassword(customerUpdatePasswordRequest);
	}
	/**
	 * 会员实名信息列表查询
	 * @author centerroot
	 * @time 创建时间:2018年9月25日下午4:08:01
	 * @param customerRealNameRequest
	 * @return
	 */
	@RequestMapping(value = "/queryCustRealNameList", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryCustRealNameList(@RequestBody @Valid CustomerRealNameRequest customerRealNameRequest) {
		return customerBaseBiz.queryCustRealNameList(customerRealNameRequest);
	}
	
	
	/**
	 * 小程序设置交易密码
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/wechatSetupPayPassword", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> wechatSetupPayPassword(@RequestBody @Valid WechatSetupPasswordRequest wechatSetupPasswordRequest) {
		return customerBaseBiz.wechatSetupPayPassword(wechatSetupPasswordRequest);
	}


	/**
	 * 小程序修改交易密码
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/wechatUpdatePayPassword", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> wechatUpdatePayPassword(@RequestBody @Valid WechatUpdatePasswordRequest wechatUpdatePasswordRequest) {
		return customerBaseBiz.wechatUpdatePayPassword(wechatUpdatePasswordRequest);
	}

}
