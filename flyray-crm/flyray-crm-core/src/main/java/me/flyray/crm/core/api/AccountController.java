package me.flyray.crm.core.api;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.msg.BizResponseCode;
import me.flyray.crm.core.biz.platform.PlatformCoinCustomerBiz;
import me.flyray.crm.core.entity.PersonalBase;
import me.flyray.crm.core.entity.PlatformCoinCustomer;
import me.flyray.crm.facade.request.*;
import me.flyray.crm.facade.response.AccountJournalQueryResponse;
import me.flyray.crm.facade.response.CustomerAccountQueryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.common.util.EntityUtils;
import me.flyray.common.util.ResponseHelper;
import me.flyray.crm.core.biz.customer.CustomerAccountQueryBiz;
import me.flyray.crm.core.biz.personal.PersonalAccountBiz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags="账户管理")
@Controller
@RequestMapping("account")
public class AccountController {
	
	@Autowired
	private PlatformCoinCustomerBiz platformCoinCustomerBiz;
	@Autowired
	private CustomerAccountQueryBiz commonAccountQueryBiz;
	@Autowired
	private PersonalAccountBiz personalAccountBiz;
	
	/**
	 * 微信小程序火源账户信息查询
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("微信小程序火源账户信息查询")
	@RequestMapping(value = "/queryFireSourceInfo",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> queryFireSourceInfo(@RequestBody @Valid QueryFireSourceParam queryFireSourceParam) throws Exception {
		PlatformCoinCustomer platformCoinCustomer = platformCoinCustomerBiz.wechatMiniFireSourceInfo(EntityUtils.beanToMap(queryFireSourceParam));
		return ResponseHelper.success(platformCoinCustomer, null, BizResponseCode.OK.getCode(), BizResponseCode.OK.getMessage());
    }
	
	/**
	 * 账户查询
	 * @param 
	 */
	@ApiOperation("账户查询")
	@RequestMapping(value = "/accountQuery",method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse accountQuery(@RequestBody @Valid AccountQueryRequest accountQueryRequest){
		List<CustomerAccountQueryResponse> response = commonAccountQueryBiz.accountQuery(accountQueryRequest);
		return BaseApiResponse.newSuccess(response);
    }
	
	/**
	 * 查询账户交易流水
	 * @param accountJournalQueryRequest
	 * @return
	 */
	@ApiOperation("查询账户交易流水")
	@RequestMapping(value = "/accountJournalQuery",method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse<List<AccountJournalQueryResponse>> accountJournalQuery(@RequestBody @Valid AccountJournalQueryRequest accountJournalQueryRequest){
		List<AccountJournalQueryResponse> response = commonAccountQueryBiz.accountJournalQuery(accountJournalQueryRequest);
		return BaseApiResponse.newSuccess(response);
    }
	
	/**
	 * 火源账户钱包创建
	 * @param 
	 */
	@ApiOperation("火源账户钱包创建")
	@RequestMapping(value = "/createFireSourceWallet",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> createFireSourceWallet(@RequestBody @Valid CreateFireSourceWalletRequest createFireSourceWalletRequest){
		PersonalBase personalBase = platformCoinCustomerBiz.createFireSourceWallet(EntityUtils.beanToMap(createFireSourceWalletRequest));
		if(null != personalBase){
			return ResponseHelper.success(null, null, BizResponseCode.OK.getCode(), BizResponseCode.OK.getMessage());
		}else{
			return ResponseHelper.success(null, null, BizResponseCode.CUST_NOTEXIST.getCode(), BizResponseCode.CUST_NOTEXIST.getMessage());
		}
    }
	
	/**
	 * 原力值账户前三排名用户信息查询
	 * @param 
	 */
	@ApiOperation("原力值账户前三排名用户信息查询")
	@RequestMapping(value = "/forceTopAccountInfo",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> forceTopAccountInfo(@RequestBody @Valid ForceTopAccountRequest forceTopAccountRequest){
		List<Map<String, Object>> respList = personalAccountBiz.forceTopAccountInfo(EntityUtils.beanToMap(forceTopAccountRequest));
		return ResponseHelper.success(respList, null, BizResponseCode.OK.getCode(), BizResponseCode.OK.getMessage());
	}

}
