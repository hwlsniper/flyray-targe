package me.flyray.crm.facade.response.personal;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import me.flyray.common.entity.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


/**
 * 个人客户信息
 * @author centerroot
 * @time 创建时间:2018年8月14日下午2:47:21
 * @description
 */

@Data
public class PersonalInfo extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//序号
    private Integer id;
    
	    //个人客户编号
    private String personalId;
	
	    //平台编号
    private String platformId;
	
	    //用户编号
    private String customerId;
	
	    //第三方会员编号
    private String thirdNo;
	
	    //手机号
	private String phone;
	
	    //用户名称
    private String realName;
	
	    //身份证号
    private String idCard;
	
	    //用户昵称
    private String nickname;
	
	    //性别 1：男 2：女
    private String sex;
	
	    //生日
    private String birthday;
	
	    //居住地
    private String address;
	
	    //身份证正面
    private String idPositive;
	
	    //身份证反面
    private String idNegative;
	
	    //认证状态  00：未认证  01：无需认证  02：认证成功  03：认证失败
    private String authenticationStatus;
	
	    //账户状态 00：正常，01：冻结
	//1:未激活 2:已激活 3:客户信息未补全
    private String status;
	
	    //创建时间
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
	
	    //更新时间
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
	
    // 归属人（存后台管理系统登录人员id）指谁发展的客户
    private Long owner;

    // 用户头像
    private String avatar;
    
	    //备注
	private String remark;
    //个人客户类型  00：注册用户  01：运营添加用户
	private String personalType;
    
	    //客户等级
    private Integer personalLevel;
	
	    //行业
    private String industry;
	
	    //行业描述
    private String industryDescription;
	
	    //职务
    private String job;
	
	    //所在省市
    private String provinces;
	
	    //电子邮件
    private String email;
	
	    //邮编
    private String zipCode;

	//TODO 最好的设计是一个list包含账户类型 余额
	private String balance;

}
