package me.flyray.crm.facade.response;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: bolei
 * @date: 10:50 2019/1/18
 * @Description: 账户流水查询
 */

@Data
public class AccountJournalQueryResponse implements Serializable {

    private String platformId;

    //商户编号
    private String merchantId;

    private String merchantName;

    //个人信息编号
    private String personalId;

    //来向账户
    private String fromAccount;

    //来向账户名称
    private String fromAccountName;

    //去向账户
    private String toAccount;

    //去向账户名称
    private String toAccountName;

    //账户类型    ACC001：余额账户
    private String accountType;

    //订单号
    private String orderNo;

    //来往标志  1：来账   2：往账
    private String inOutFlag;

    //交易金额
    private BigDecimal tradeAmt;

    //交易类型  支付:01，退款:02，提现:03，充值:04
    private String tradeType;

    //创建时间
    private Date createTime;

    //更新时间
    private Date updateTime;

    /**
     * 积分使用场景
     */
    private String sceneId;
}
