package me.flyray.admin.rest;

import me.flyray.admin.biz.UserRoleBiz;
import me.flyray.admin.entity.UserRole;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.rest.BaseController;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *　@author bolei
 *　@date Apr 8, 2018
　*　@description 
**/

@RestController
@RequestMapping("userRole")
public class UserRoleController extends BaseController<UserRoleBiz, UserRole> {
	
	/**
     * 通过用户ID获取用户角色
     * @param id
     * @return
     */
    @RequestMapping(value = "/{userId}/roles", method = RequestMethod.GET)
    @ResponseBody
    public BaseApiResponse<List<UserRole>> getUserRoles(@PathVariable String userId){
    	Long usId = Long.valueOf(userId);
        return new BaseApiResponse<List<UserRole>>(baseBiz.getUserRolesByUserId(usId));
        
    }
}
