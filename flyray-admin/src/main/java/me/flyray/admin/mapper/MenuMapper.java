package me.flyray.admin.mapper;

import me.flyray.admin.entity.Menu;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface MenuMapper extends Mapper<Menu> {

    /**
     * 查询角色在应用下的菜单权限
     * @param authorityId
     * @param authorityType
     * @return
     */
    public List<Menu> selectMenuByAuthorityIdAppId(@Param("authorityId") String authorityId,@Param("appId") String appId,@Param("authorityType") String authorityType);

    /**
     * 根据用户和组的权限关系查找用户可访问菜单
     * @param userId
     * @return
     */
    public List<Menu> selectAuthorityMenuByUserId (@Param("userId") String userId);

    /**
     * 根据用户和组的权限关系查找用户可访问的系统
     * @param userId
     * @return
     */
    public List<Menu> selectAuthoritySystemByUserId (@Param("userId") int userId);

    /**
     * 根据用户和appId的权限关系查找用户可访问的系统
     * @param userId
     * @param appId
     * @return
     */
    public List<Menu> getAuthorityAppMenuByUserIdAppId (@Param("userId") String userId,@Param("appId") String appId);

}